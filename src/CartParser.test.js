import { parse } from 'uuid';
import CartParser from './CartParser';

let parser;

beforeEach(() => {
    parser = new CartParser();
});

describe('CartParser - unit tests', () => {
    // Add your unit tests here.
    it('corectly works calcTotal function', () => {
        const cartItems = [
            {
                price: 9,
                quantity: 2,
            },
            {
                price: 10.32,
                quantity: 1,
            },
        ];

        const result = parser.calcTotal(cartItems);

        expect(result).toBe(28.32);
    });

    it('calcTotal function return 0 if array empty', () => {
        const cartItems = [];

        const result = parser.calcTotal(cartItems);

        expect(result).toBe(0);
    });

    it('corectly works parseLine function', () => {
        const stringLine = 'Tvoluptatem,10.32,1';

        const result = parser.parseLine(stringLine);

        expect(result.name).toContain('Tvoluptatem');
        expect(result.price).toEqual(10.32);
        expect(result.quantity).toEqual(1);
    });

    it('corectly works readfile function', () => {
        const path = './samples/cart.json';

        const result = parser.readFile(path);

        expect(result).toBeTruthy();
    });

    it('validate function doesnt return errors', () => {
        const data = 'Product name,Price,Quantity\nMollis consequat,9.00,2\nTvoluptatem,10.32,1';

        const result = parser.validate(data);

        expect(result).toEqual([]);
    });

    it('validate function return error with wrong header', () => {
        const data = 'Price,Quantity\nMollis consequat,9.00,2\nTvoluptatem,10.32,1';

        const result = parser.validate(data);
        const error = { type: 'header' };

        expect(error).not.toEqual(expect.arrayContaining(result));
    });

    it('validate function throw error', () => {
        const data = `{\n
			"items": [\n
				{\n
					"id": "3e6def17-5e87-4f27-b6b8-ae78948523a9",\n
					"name": "Mollis consequat",
					"price": 9,\n
					"quantity": 2\n
				} ]}`;

        const validationErrors = parser.validate(data);

        expect(validationErrors.length).toBeGreaterThan(0);
    });

    it('validate function return expected error', () => {
        const data = `{\n
			"items": [\n
				{\n
					"id": "3e6def17-5e87-4f27-b6b8-ae78948523a9",\n
					"name": "Mollis consequat",
					"price": 9,\n
					"quantity": 2\n
				} ]}`;

        const result = parser.validate(data);
        const expectedError = {
            type: 'header',
            row: 0,
            column: 1,
            message: 'Expected header to be named "Price" but received undefined.',
        };

        expect(result).toContainEqual(expectedError);
    });

    it('should create validate error', () => {
        const data = `{\n
			"items": [\n
				{\n
					"id": "3e6def17-5e87-4f27-b6b8-ae78948523a9",\n
					"name": "Mollis consequat",
					"price": 9,\n
					"quantity": 2\n
				} ]}`;

        const result = parser.validate(data);

        expect(result[0]).toHaveProperty('type', 'row', 'column', 'message');
    });
});

describe('CartParser - integration test', () => {
    it('some test', () => {
        const path = './samples/cart.csv';

        const result = parser.parse(path);

        expect(result.total).toBe(348.32);
        expect(result.items[0]).toHaveProperty('name', 'price', 'quantity', 'id');
    });
    // Add your integration test here.
});
